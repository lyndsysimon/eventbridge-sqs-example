# EventBridge -> SQS Illustration

This repo shows the way SQS targets actually work in AWS EventBridge, versus how
I expected them to work based on my experience with other types of targets - in
particular, API destinations.

My initial expectation was that the ability for a resource to send messages to
the queue could be enabled by allowing the resource to assume an IAM role with
the ability to do so. This is not the case - EventBridge targets for an SQS
queue do not allow you to specify a role. This limitation became apparent to me
when I attempted to apply a Terraform plan with a `role_arn` argument provided
to an `aws_cloudwatch_event_target` resource. I confirmed that when editing the
target in the AWS Console the option to provide a role is no preset; I don't
believe this is a limitation in either Terraform's `aws` provider or in
`aws-cli`, which it is based upon.

Instead, the only way I've found to successfully send messages to SQS from
EventBridge is to set the SQS queue's "queue policy". This is an attribute of
the queue itself. To my knowledge there is no way to read the
current state of the queue policy once it has been set.

As a result of the queue policy being "write-only", I've been unable to find a
way to "extend" the existing policy to allow for subsequently-created targets to
send messages to the queue. As a work-around, I've taken the following approach:

* We have an internal Terraform module that can be used to define rule+target
  combinations in a single resource. This module exposes the policy fragment
  needed to allow the target to send messages to the queue as a JSON object.
* The SQS queue policy is set once, in the same Terraform module where the queue
  itself is defined.
* To determine the final policy, all modules where EventBridge targets are
  defined that send to the queue are imported as a remote state object
* From each remote state object, the policy fragment JSON object is an output
* A "composite" policy document is defined, combining all of the above policy
  fragments. _See [Terraform AWS provider docs][]._

This isn't limited to EventBridge. If the SQS queue policy is defined through
any other means, it will be overwritten when the `aws_sqs_queue_policy` resource
is applied. Because the queue policy is read-only, there seems to be no way to
ensure that setting it is ever truly safe; you could be overwriting changes made
elsewhere.

Finally, I note that adding a target through the AWS Console _modifies_ the
existing SQS queue policy. Removing the target reverts the queue policy back to
the previous state. Either this is using an internal API that I don't have
access to, or I've missed a means of reading the existing queue policy.

[Terraform AWS provider docs]: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document#source_policy_documents

## Repository Layout

| File            | Contents                                          |
| --------------- | ------------------------------------------------- |
| `base.tf`       | Terraform configuration                           |
| `bus.tf`        | Example event bus used for both examples          |
| `working.tf`    | Working implementation, using an SQS queue policy |
| `notworking.tf` | Non-working implementation, using an IAM role     |

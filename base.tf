terraform {
  required_version = "1.2.3"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "local" {
    path = ".tfstate"
  }
}

provider "aws" {
  region = "ca-central-1"
  default_tags {
    tags = {
      Environment : "eventbridge-sqs-example"
    }
  }
}

locals {
  # namespace
  ns = "sqs-example"
}

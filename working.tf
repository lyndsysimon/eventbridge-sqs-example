resource "aws_cloudwatch_event_rule" "working" {
  name = "${local.ns}-working"
  event_bus_name = aws_cloudwatch_event_bus.bus.name
  event_pattern = jsonencode({
    detail-type = ["Test"]
  })
}

resource "aws_cloudwatch_event_target" "working" {
  rule = aws_cloudwatch_event_rule.working.name
  event_bus_name = aws_cloudwatch_event_bus.bus.name
  arn = aws_sqs_queue.working.arn
}

resource "aws_sqs_queue" "working" {
  name = "${local.ns}-working"
}

data "aws_iam_policy_document" "working" {
  statement {
    effect = "Allow"
    actions = ["sqs:SendMessage"]
    resources = [aws_cloudwatch_event_target.working.arn]
    principals {
      type = "Service"
      identifiers = ["events.amazonaws.com"]
    }
    condition {
      test = "ArnEquals"
      variable = "aws:SourceArn"
      values = [aws_cloudwatch_event_rule.working.arn]
    }
  }
}

resource "aws_sqs_queue_policy" "working" {
  queue_url = aws_sqs_queue.working.id
  policy = data.aws_iam_policy_document.working.json
}

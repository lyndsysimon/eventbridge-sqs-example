resource "aws_cloudwatch_event_rule" "notworking" {
  name = "${local.ns}-notworking"
  event_bus_name = aws_cloudwatch_event_bus.bus.name
  event_pattern = jsonencode({
    detail-type = ["Test"]
  })
}

resource "aws_cloudwatch_event_target" "notworking" {
  rule = aws_cloudwatch_event_rule.notworking.name
  event_bus_name = aws_cloudwatch_event_bus.bus.name
  arn = aws_sqs_queue.notworking.arn
  # I expected to be able to use the following to attach a role to the target -
  # but could not do so.
  #role_arn = aws_iam_role.notworking.arn
}

resource "aws_sqs_queue" "notworking" {
  name = "${local.ns}-notworking"
}

resource "aws_iam_role" "notworking" {
  name = "${local.ns}-notworking"
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy_attachment" "notworking" {
  role = aws_iam_role.notworking.name
  policy_arn = aws_iam_policy.notworking.arn
}

resource "aws_iam_policy" "notworking" {
  name = "${local.ns}-notworking"
  policy = data.aws_iam_policy_document.sqs_access.json
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "sqs_access" {
  statement {
    effect = "Allow"
    actions = ["sqs:SendMessage"]
    resources = [aws_sqs_queue.notworking.arn]
  }
}
